﻿using System.Collections.Generic;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Persistence;
using Assets.Scripts.Persistence.DTOs;
using UnityEditor;
using UnityEngine;
using WeaponAttachmentData = Assets.Scripts.Persistence.DTOs.WeaponAttachmentData;

namespace Assets.Scripts.GameCommon
{
    public class DataFilesCreator : MonoBehaviour
    {
        [MenuItem("FileSystem / Create / Create Weapon Data")]
        public static void CreateWeaponData()
        {
            var allWeaponStats = new List<WeaponStats>();
            allWeaponStats.Add(lightGunStats);
            allWeaponStats.Add(heavyGunStats);
            allWeaponStats.Add(autoShotgunStats);
            allWeaponStats.Add(smgStats);
            allWeaponStats.Add(dmrStats);
            allWeaponStats.Add(shotgunStats);

            FileSystem.SaveWeaponData(allWeaponStats.ToArray());
        }

        [MenuItem("FileSystem / Create / Create Attachment Data")]
        public static void CreateAttachmentData()
        {
            var allAttachmentStats = new List<ModifierSet>();
            allAttachmentStats.Add(blinderStats);
            allAttachmentStats.Add(gripStats);
            allAttachmentStats.Add(laserStats);
            allAttachmentStats.Add(redDotStats);
            allAttachmentStats.Add(reflexStats);
            allAttachmentStats.Add(shieldStats);
            allAttachmentStats.Add(armorPiercingStats);
            allAttachmentStats.Add(chargedRoundStats);
            allAttachmentStats.Add(quickReleaseStats);
            allAttachmentStats.Add(extendedMagStats);

            FileSystem.SaveAttachmentData(allAttachmentStats.ToArray());
        }

        [MenuItem("FileSystem / Create / Create Loadout Data")]
        public static void CreateLoadout()
        {
            var attachments = new List<WeaponAttachmentData>();
            attachments.Add(new WeaponAttachmentData {WeaponId = "light-gun", Attachments = new string[0]});
            attachments.Add(new WeaponAttachmentData { WeaponId = "heavy-gun", Attachments = new string[0] });
            attachments.Add(new WeaponAttachmentData { WeaponId = "auto-shotgun", Attachments = new string[0] });
            attachments.Add(new WeaponAttachmentData { WeaponId = "dmr", Attachments = new string[0] });
            attachments.Add(new WeaponAttachmentData { WeaponId = "smg", Attachments = new string[0] });

            var loadout = new LoadoutData
            {
                PrimaryWeaponId = "light-gun",
                SecondaryWeaponId = "light-gun",
                AttachmentInfo = attachments.ToArray()
            };

            FileSystem.SaveLoadout(loadout);
        }

        #region weapons
        static WeaponStats lightGunStats = new WeaponStats
        {
            WeaponId = "light-gun",
            CycleRate = .3f,
            AimSpeed = .3f,
            Accuracy = 55,
            AimBonus = 15,
            Recoil = new Recoil {Violence = .4f, Tendency = new Vector2(-2, 1)},
            MagazineSize = 15,
            Damage = new Damage(15),
            ReloadTime = 1,
            TunnelVisionFactor = .2f,
            AimZoom = 10,
            WeaponHandCount = 1,
            IsAutomatic = false,
            PushForce = 3800
        };
        static WeaponStats heavyGunStats = new WeaponStats
        {
            WeaponId = "heavy-gun",
            CycleRate = .3f,
            AimSpeed = .2f,
            Accuracy = 65,
            AimBonus = 20,
            Recoil = new Recoil { Violence = .6f, Tendency = new Vector2(-2, 1) },
            MagazineSize = 7,
            Damage = new Damage(23),
            ReloadTime = 1.3f,
            TunnelVisionFactor = .4f,
            AimZoom = 20,
            WeaponHandCount = 1,
            IsAutomatic = false,
            PushForce = 4500
        };
        static WeaponStats autoShotgunStats = new WeaponStats
        {
            WeaponId = "auto-shotgun",
            CycleRate = .3f,
            AimSpeed = .1f,
            Accuracy = 15,
            AimBonus = 10,
            Recoil = new Recoil { Violence = .8f, Tendency = new Vector2(-3, 1) },
            MagazineSize = 6,
            Damage = new Damage(80),
            ReloadTime = 2f,
            TunnelVisionFactor = .3f,
            AimZoom = 10,
            WeaponHandCount = 2,
            IsAutomatic = false,
            PushForce = 3000
        };
        static WeaponStats smgStats = new WeaponStats
        {
            WeaponId = "smg",
            CycleRate = .1f,
            AimSpeed = .15f,
            Accuracy = 35,
            AimBonus = 20,
            Recoil = new Recoil { Violence = .4f, Tendency = new Vector2(-2, 1.5f) },
            MagazineSize = 30,
            Damage = new Damage(12),
            ReloadTime = 2.1f,
            TunnelVisionFactor = .3f,
            AimZoom = 15,
            WeaponHandCount = 2,
            IsAutomatic = true,
            PushForce = 3000
        };
        static WeaponStats dmrStats = new WeaponStats
        {
            WeaponId = "dmr",
            CycleRate = .3f,
            AimSpeed = .1f,
            Accuracy = 80,
            AimBonus = 20,
            Recoil = new Recoil { Violence = .6f, Tendency = new Vector2(-3, 1) },
            MagazineSize = 10,
            Damage = new Damage(46),
            ReloadTime = 2.5f,
            TunnelVisionFactor = .8f,
            AimZoom = 30,
            WeaponHandCount = 2,
            IsAutomatic = false,
            PushForce = 5000
        };
        static WeaponStats shotgunStats = new WeaponStats
        {
            WeaponId = "shotgun",
            CycleRate = .6f,
            AimSpeed = .1f,
            Accuracy = 10,
            AimBonus = 10,
            Recoil = new Recoil { Violence = .8f, Tendency = new Vector2(-3, 1) },
            MagazineSize = 8,
            Damage = new Damage(60),
            ReloadTime = 2.6f,
            TunnelVisionFactor = .3f,
            AimZoom = 10,
            WeaponHandCount = 2,
            IsAutomatic = false,
            PushForce = 3000
        };
        #endregion

        #region attachments
        static ModifierSet blinderStats = new ModifierSet
        {
            AttachmentId = "blinder",
            AimSpeed = -0.01f,
            ReloadTime = 0.05f,
            RecoilViolence = 0.2f
        };
        static ModifierSet gripStats = new ModifierSet
        {
            AttachmentId = "grip",
            AimSpeed = -0.01f,
            AimBonus = 5,
            Accuracy = 5,
            RecoilViolence = -0.2f
        };
        static ModifierSet laserStats = new ModifierSet
        {
            AttachmentId = "laser",
            AimSpeed = -0.01f,
            ReloadTime = 0.05f,
            Accuracy = 15,
            RecoilViolence = 0.02f
        };
        static ModifierSet redDotStats = new ModifierSet
        {
            AttachmentId = "red-dot",
            AimZoom = 10,
            AimSpeed = -0.03f,
            AimBonus = 25,
            TunnelVisionFactor = 0.5f
        };
        static ModifierSet reflexStats = new ModifierSet
        {
            AttachmentId = "reflex",
            AimZoom = 5,
            AimSpeed = -0.05f,
            AimBonus = 20,
            TunnelVisionFactor = 0.1f
        };
        static ModifierSet shieldStats = new ModifierSet
        {
            AttachmentId = "shield",
            AimSpeed = -0.01f,
            TunnelVisionFactor = 0f,
            ReloadTime = 0.15f,
            RecoilViolence = 0.2f
        };
        static ModifierSet armorPiercingStats = new ModifierSet
        {
            AttachmentId = "armor-piercing",
            Damage = new DamageModifier
            {
                FleshDamageMultiplier = 0.9f,
                ArmorDamageMultiplier = 1.7f,
                ElectricDamageMultiplier = 0.3f
            }
        };
        static ModifierSet chargedRoundStats = new ModifierSet
        {
            AttachmentId = "charged-round",
            Damage = new DamageModifier
            {
                FleshDamageMultiplier = 0.8f,
                ArmorDamageMultiplier = 0.2f,
                ElectricDamageMultiplier = 1.8f
            }
        };
        static ModifierSet quickReleaseStats = new ModifierSet
        {
            AttachmentId = "quick-release",
            ReloadTime = -0.3f,
        };
        static ModifierSet extendedMagStats = new ModifierSet
        {
            AttachmentId = "extended-mag",
            ReloadTime = 0.1f,
            MagazineMultiplier = 1.2f,
        };
        #endregion
    }
}
