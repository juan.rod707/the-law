﻿using System;

namespace Assets.Scripts.Persistence.DTOs
{
    [Serializable]
    public struct WeaponAttachmentData
    {
        public string WeaponId;
        public string[] Attachments;
    }
}
