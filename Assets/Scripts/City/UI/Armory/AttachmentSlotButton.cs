﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.City.UI.Armory
{
    public class AttachmentSlotButton: MonoBehaviour
    {
        public AttachmentType SlotType;
        public GameObject OptionContainer;
        public Text Icon;

        private Action<AttachmentType, string> onChanged;
        private Dictionary<string, string> optionSpriteMap;

        public void Initialize(Action<AttachmentType, string> onChanged)
        {
            optionSpriteMap = new Dictionary<string, string>();
            this.onChanged = onChanged;
            foreach (var ab in OptionContainer.GetComponentsInChildren<AttachmentButton>())
            {
                optionSpriteMap.Add(ab.AttachmentId, ab.Icon.text);
                ab.Initialize(SelectAttachment);
            }

            HideOptions();
        }

        public void OnClick() => ShowOptions();

        private void HideOptions() => OptionContainer.SetActive(false);
        private void ShowOptions() => OptionContainer.SetActive(true);

        void SelectAttachment(string attachmentId, string sprite)
        {
            Icon.text = sprite;
            onChanged(SlotType, attachmentId);
            HideOptions();
        }

        public void SetTo(IEnumerable<string> attachments)
        {
            if (optionSpriteMap.Keys.Intersect(attachments).Any())
                Icon.text = optionSpriteMap[optionSpriteMap.Keys.Intersect(attachments).First()];
            else
                Icon.text = optionSpriteMap.First().Value;
        }
    }
}
