﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.City.UI.Armory
{
    public class AttachmentButton : MonoBehaviour
    {
        public string AttachmentId;
        public Text Icon;
        
        private Action<string, string> doOnClick;

        public void Initialize(Action<string, string> doOnClick) => 
            this.doOnClick = doOnClick;

        public void OnClick() => 
            doOnClick(AttachmentId, Icon.text);
    }
}
