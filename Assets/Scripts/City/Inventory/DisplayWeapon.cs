﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Persistence;
using UnityEngine;

namespace Assets.Scripts.City.Inventory
{
    public class DisplayWeapon : MonoBehaviour
    {
        public string WeaponId;
        public WeaponAttachmentData AttachmentData;
        
        [HideInInspector]
        public WeaponStats CurrentStats;
        WeaponStats BaseStats;
        IEnumerable<DisplayAttachment> EquipedAttachments => GetComponentsInChildren<DisplayAttachment>();

        public IEnumerable<string> GetAttachmentSerializableData =>
            EquipedAttachments.Select(a => a.AttachmentId);

        private IEnumerable<ModifierSet> modifierSets => EquipedAttachments.Select(da => da.ModifierSet);

        public void Initialize()
        {
            BaseStats = InventoryDataPersistance.GetBaseWeaponStats(WeaponId);
            ReloadStats();
        }

        void ReloadStats()
        {
            CalculateCurrentStats(modifierSets);
        }

        public void AddAttachment(DisplayAttachment attachment)
        {
            RemoveAttachment(attachment.AttachmentType);

            var parent = AttachmentData.AttachmentContainer;

            if (attachment.AttachmentType == AttachmentType.Optic)
                parent = AttachmentData.OpticSpot;
            else if (attachment.AttachmentType == AttachmentType.UnderBarrel)
                parent = AttachmentData.UnderBarrelSpot;

            attachment.transform.SetParent(parent);
            attachment.transform.localPosition = Vector3.zero;
            attachment.transform.localRotation = Quaternion.identity;
            ReloadStats();
        }

        public void RemoveAttachment(AttachmentType slotType)
        {
            var slotAttachments = EquipedAttachments.Where(a => a.AttachmentType == slotType);
            foreach(var sa in slotAttachments)
                Destroy(sa.gameObject);
            ReloadStats();
        }

        protected virtual void CalculateCurrentStats(IEnumerable<ModifierSet> modifierSets)
        {
            CurrentStats = BaseStats;
            foreach (var ms in modifierSets)
            {
                CurrentStats.ReloadTime += ms.ReloadTime;
                CurrentStats.AimBonus += ms.AimBonus;
                CurrentStats.AimSpeed += ms.AimSpeed;
                CurrentStats.AimZoom += ms.AimZoom;
                CurrentStats.TunnelVisionFactor += ms.TunnelVisionFactor;
                CurrentStats.Accuracy += ms.Accuracy;
                CurrentStats.Recoil.Violence += ms.RecoilViolence;
                if (ms.MagazineMultiplier > 0)
                    CurrentStats.MagazineSize = (int)(CurrentStats.MagazineSize * ms.MagazineMultiplier);
                CurrentStats.Damage.ApplyModifier(ms.Damage);
            }
        }
    }
}
