﻿using System.Linq;
using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.City.Inventory
{
    public class DisplayAttachmentFactory : MonoBehaviour
    {
        public DisplayAttachment[] AttachmentPrefabs;

        public DisplayAttachment CreateAttachment(string attachmentId)
        {
            var attachmentPrefab = AttachmentPrefabs.First(a => a.AttachmentId.MatchId(attachmentId));
            var attachment = Instantiate(attachmentPrefab);
            attachment.Initialize();

            return attachment;
        }
    }
}
