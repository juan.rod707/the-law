﻿using System.Collections.Generic;
using System.Linq;
using LevelGenerator.Scripts.Helpers;
using UnityEngine;

namespace Assets.Scripts.City
{
    public class BuildingProvider : MonoBehaviour
    {
        private IList<Building> buildings;
        public void Initialize() => buildings = GetComponentsInChildren<Building>().ToList();

        public Building GetRandomBuilding()
        {
            var candidate = buildings
                .Where(b => b.transform.position.magnitude < 600)
                .PickOne();

            buildings.Remove(candidate);
            return candidate;
        }
    }
}