﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.City.Life
{
    public class CityNavPointProvider : MonoBehaviour
    {
        private IEnumerable<CityNavPoint> navPoints;
        public int TotalNavPoints => GetComponentsInChildren<CityNavPoint>().Length;

        public void Initialize() => navPoints = GetComponentsInChildren<CityNavPoint>();

        public CityNavPoint GetRandomNavPoint() => navPoints.PickOne();

        public CityNavPoint GetFromName(string name) => GetComponentsInChildren<CityNavPoint>().First(p => p.gameObject.name.Equals(name));
    }
}
