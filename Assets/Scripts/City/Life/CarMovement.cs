﻿using System;
using Assets.Scripts.GameCommon;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.City.Life
{
    public class CarMovement : MonoBehaviour
    {
        public float ClosenessThreshold;
        public float MinSpeed;
        public float MaxSpeed;
        public float TurnRate;

        public Gradient[] CarColors;
        public ParticleSystem Vfx;

        private float speed;
        private CityNavPoint nextPoint;
        private CityNavPoint lastPoint;

        public void Initialize(CityNavPointProvider navPoints, CityNavPoint initialPoint)
        {
            speed = Random.Range(MinSpeed, MaxSpeed);
            nextPoint = initialPoint.GetRandomNeighbour(initialPoint);
            lastPoint = initialPoint;
            var vfx = Vfx.main;
            vfx.startColor = CarColors.PickOne();
        }

        void ChangeTarget()
        {
            try
            {
                var currentPoint = nextPoint;
                nextPoint = nextPoint.GetRandomNeighbour(lastPoint);
                lastPoint = currentPoint;

            }
            catch
            {
                Debug.Log($"failed to get neighbour without exception on node {nextPoint.gameObject.name}");
                var currentPoint = nextPoint;
                nextPoint = nextPoint.GetRandomNeighbour();
                lastPoint = currentPoint;
            }

        }

        void Update()
        {
            if (nextPoint)
            {
                MoveForward();
                Steer(nextPoint.Position);

                if (Vector3.Distance(transform.position, nextPoint.Position) < ClosenessThreshold)
                    ChangeTarget();
            }
        }

        private void MoveForward() => transform.Translate(Vector3.forward * speed * Time.deltaTime);

        void Steer(Vector3 target)
        {
            var relativeTarget = transform.InverseTransformPoint(target);
            
            transform.Rotate(Vector3.up * TurnRate * relativeTarget.x * Time.deltaTime);
            transform.Rotate(Vector3.right * TurnRate * -relativeTarget.y * Time.deltaTime);
            NormalizeRoll();
        }

        private void NormalizeRoll()
        {
            var euler = transform.eulerAngles;
            euler.z = 0f;
            transform.eulerAngles = euler;
        }
    }
}
