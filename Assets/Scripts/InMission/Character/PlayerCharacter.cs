﻿using System;
using System.Collections.Generic;
using Assets.Scripts.InMission.Actor;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.InMission.Weapons.ShoulderCannon;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.InMission.Character
{
    public class PlayerCharacter : MonoBehaviour
    {
        public HeadController Head;
        public CharacterLook Look;
        public WeaponController Weapon;
        public CharacterMovement Movement;
        public Humanoid Humanoid;
        public Health Health;
        public Animator Animator;
        public NavMeshAgent Agent;
        public CharacterInteraction Interaction;
        public ShoulderCannon ShoulderCannon;

        private HUD hud;
        

        public void Initialize(HUD hud, Transform startSpot, IEnumerable<Weapon> equippedWeapons, Action onDeath)
        {
            this.hud = hud;
            Agent.Warp(startSpot.position);
            transform.rotation = startSpot.rotation;

            Health.Initialize(OnGetHit, onDeath);
            hud.Initialize(Health.InitialHp);
            Head.Initialize();
            Weapon.Initialize(Head, hud.TunnelVision, equippedWeapons);
            Movement.Initialize(Animator);
        }

        private void OnGetHit(int maxHp, int currentHp, int damage)
        {
            hud.HurtIndicator.FlashEffect(damage);
            hud.UpdateHealth(maxHp, currentHp);
            Head.Shake(0.5f, Vector2.zero, 18);
        }

        public void Die()
        {
            hud.HurtIndicator.ShowPermanent();
            Weapon.Hide();
            Animator.applyRootMotion = false;
            Animator.SetTrigger("Death");
        }
    }
}
