﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InMission.Character
{
    public class HeadController : MonoBehaviour
    {
        public Camera Camera;
        public Transform ViewPoint;

        private float baseFov;
        private float aimSpeed;
        private float targetFov;
        private float currentFov;

        public void Initialize()
        {
            baseFov = Camera.fieldOfView;
            currentFov = baseFov;
            targetFov = currentFov;
        }

        void Update()
        {
            if (aimSpeed > 0)
            {
                currentFov = Mathf.Lerp(currentFov, targetFov, aimSpeed);
                Camera.fieldOfView = currentFov;
            }
        }

        public void Shake(float violence, Vector2 tendency, int frames) => 
            StartCoroutine(DoShake(violence, tendency, frames));

        public void ShakeFrame(float violence, Vector2 tendency) =>
            Shake(violence, tendency, 1);

        public void AimEffect(float fovChange, float speed)
        {
            aimSpeed = speed;
            this.targetFov = baseFov - fovChange;
        }

        public void ResetView(float speed)
        {
            aimSpeed = speed;
            this.targetFov = baseFov;
        }

        IEnumerator DoShake(float violence, Vector2 tendency, int frames)
        {
            var elapsed = 0;
            while (elapsed < frames)
            {
                elapsed ++;
                var euler = transform.eulerAngles;
                var chaos = (Random.insideUnitCircle + tendency) * violence;
                var yFactor = Random.Range(0, 2) > 0 ? 1f : -1f;
                transform.eulerAngles = euler + new Vector3(chaos.x, chaos.y * yFactor, 0f);

                yield return null;
            }
        }
    }
}
