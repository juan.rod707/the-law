﻿using Assets.Scripts.InMission.Actor;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons.Attachments
{
    public class Blinder : MonoBehaviour
    {
        public LayerMask EyesLayer;
        public float BlindingStrength;
        public float StunDuration;
        public Transform Emitter;

        void Update()
        {
            var ray = new Ray(Emitter.position, transform.forward);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100f, EyesLayer))
            {
                var eyes = hit.collider.GetComponent<Eyes>();
                if(eyes)
                    eyes.ApplyBlinding(BlindingStrength, StunDuration);
            }
        }
    }
}
