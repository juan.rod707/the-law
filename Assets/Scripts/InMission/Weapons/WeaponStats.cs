﻿using System;

namespace Assets.Scripts.InMission.Weapons
{
    [Serializable]
    public struct WeaponStats
    {
        public string WeaponId;
        public bool IsAutomatic;
        public int WeaponHandCount;
        public Damage Damage;
        public int MagazineSize;
        public float PushForce;
        public float AimZoom;
        public float AimSpeed;
        public float AimBonus;
        public float TunnelVisionFactor;
        public float CycleRate;
        public float ReloadTime;
        public float Accuracy;
        public Recoil Recoil;
    }
}
