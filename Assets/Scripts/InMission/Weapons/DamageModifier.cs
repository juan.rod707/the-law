﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.InMission.Weapons
{
    [Serializable]
    public struct DamageModifier
    {
        public float FleshDamageMultiplier;
        public float ArmorDamageMultiplier;
        public float ElectricDamageMultiplier;
    }
}
