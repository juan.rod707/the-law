﻿using System;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    [Serializable]
    public struct Recoil
    {
        public float Violence;
        public Vector2 Tendency;
    }
}
