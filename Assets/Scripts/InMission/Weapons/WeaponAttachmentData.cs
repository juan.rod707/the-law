﻿using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class WeaponAttachmentData : MonoBehaviour
    {
        public Transform OpticSpot;
        public Transform UnderBarrelSpot;
        public Transform AttachmentContainer => this.transform;
    }
}
