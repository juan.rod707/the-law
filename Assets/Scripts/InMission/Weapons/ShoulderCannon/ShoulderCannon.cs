﻿using UnityEngine;

namespace Assets.Scripts.InMission.Weapons.ShoulderCannon
{
    public class ShoulderCannon : MonoBehaviour
    {
        public Animator Animator;
        public StunGrenadeLauncher weapon;
        
        public void Fire()
        {
            PlayFireAnimation();
            weapon.Fire();
        }

        void PlayFireAnimation() => Animator.SetTrigger("Fire");
    }
}
