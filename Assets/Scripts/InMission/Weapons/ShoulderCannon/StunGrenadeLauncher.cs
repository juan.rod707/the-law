﻿using UnityEngine;

namespace Assets.Scripts.InMission.Weapons.ShoulderCannon
{
    public class StunGrenadeLauncher : MonoBehaviour, ShoulderWeapon
    {
        public StunGrenade GrenadePrefab;
        public ParticleSystem MuzzleFlash;
        public AudioSource FireSfx;
        
        public float FuseLength;
        public float StunDuration;
        public float LaunchStrength;
        public float StunRange;

        public void Fire()
        {
            var grenade = Instantiate(GrenadePrefab, MuzzleFlash.transform.position, transform.rotation);
            grenade.Initialize(FuseLength, StunDuration, StunRange, LaunchStrength);
            MuzzleFlash.Play();
            FireSfx.Play();
        }
    }
}
