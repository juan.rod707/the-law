﻿namespace Assets.Scripts.InMission.Weapons.ShoulderCannon
{
    public interface ShoulderWeapon
    {
        void Fire();
    }
}
