﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.InMission.Navigation
{
    public class CoverSpotProvider : MonoBehaviour
    {
        IEnumerable<CoverSpot> coverSpots;

        public void Initialize() => coverSpots = GetComponentsInChildren<CoverSpot>();

        public CoverSpot GetCoveredSpotFrom(Vector3 target) => coverSpots.First(cs => cs.IsCoveredFrom(target));

        public CoverSpot GetClosestCoveredSpotFrom(Vector3 target, Vector3 source) =>
            coverSpots
                .Where(cs => cs.IsCoveredFrom(target))
                .Select(c => new {spot = c, distance = Vector3.Distance(c.Position, source)})
                .OrderBy(csd => csd.distance)
                .First().spot;
    }
}
