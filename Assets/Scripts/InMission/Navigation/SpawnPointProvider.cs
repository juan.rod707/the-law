﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Common;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.InMission.Navigation
{
    public class SpawnPointProvider : MonoBehaviour
    {
        private IList<SpawnPoint> spawnPoints;
        private IList<BombSpawnPoint> bombSpawnPoints;

        public PlayerSpawnPoint PlayerSpwawnPoint { get; private set; }
        
        public void Initialize()
        {
            spawnPoints = GetComponentsInChildren<SpawnPoint>().ToList();
            bombSpawnPoints = GetComponentsInChildren<BombSpawnPoint>().ToList();
            PlayerSpwawnPoint = GetComponentInChildren<PlayerSpawnPoint>();
            if (!PlayerSpwawnPoint)
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public Maybe<SpawnPoint> GetSpawnPoint()
        {
            if (!spawnPoints.Any())
                return Maybe<SpawnPoint>.Empty();

            var candidate = spawnPoints.PickOne();
            spawnPoints.Remove(candidate);
            return new Maybe<SpawnPoint>(candidate);
        }

        public Maybe<BombSpawnPoint> GetBombSpawnPoint()
        {
            if (!bombSpawnPoints.Any())
                return Maybe<BombSpawnPoint>.Empty();

            var candidate = bombSpawnPoints.PickOne();
            bombSpawnPoints.Remove(candidate);
            return new Maybe<BombSpawnPoint>(candidate);
        }
    }
}
