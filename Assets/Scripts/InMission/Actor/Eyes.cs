﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class Eyes : MonoBehaviour
    {
        public float BlinderExposureTolerance;
        public float RecoveryFactor;

        private Action<float> onStun = f => { };
        private float blinderExposure;

        public void Initialize(Action<float> onStun)
        {
            this.onStun = onStun;
        }

        public void ApplyBlinding(float blindingSthrength, float stunDuration)
        {
            blinderExposure += blindingSthrength;
            if (blinderExposure > BlinderExposureTolerance)
                onStun(stunDuration);
        }

        void Update()
        {
            if (blinderExposure > 0)
            {
                blinderExposure -= RecoveryFactor;
            }
        }
    }
}
