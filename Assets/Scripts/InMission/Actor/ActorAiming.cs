﻿using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class ActorAiming : MonoBehaviour
    {
        public float TurnRate;
        public float LockInTolerance;
        public float EngageDistance;
        public LayerMask HitLayer;

        private ActorWeapon weapon;
        private ActorAnimator animator;
        private Humanoid target;
        private Humanoid self;
        private Transform viewPoint;

        private bool ReadyToFire => TargetIsInFront && (TargetIsClose || TargetVisible);

        private bool TargetIsClose => 
            Vector3.Distance(transform.position, target.CenterOfMass) < EngageDistance;

        private bool TargetIsInFront =>
            Mathf.Abs(transform.InverseTransformPoint(target.CenterOfMass).x) < LockInTolerance;

        private bool TargetVisible
        {
            get
            {
                var directionToTarget = (target.Head - self.Head).normalized;
                var ray = new Ray(self.Head, directionToTarget);
                RaycastHit hit;

                return Physics.Raycast(ray, out hit, 100f, HitLayer) &&
                       hit.collider.GetComponent<Humanoid>() &&
                       hit.collider.GetComponent<Humanoid>().Equals(target);
            }
        }

        public void Initialize(ActorWeapon weapon, ActorAnimator animator, Humanoid target, Humanoid self, Transform viewPoint)
        {
            this.self = self;
            this.weapon = weapon;
            this.animator = animator;
            this.target = target;
            this.viewPoint = viewPoint;
            enabled = false;
        }

        void Update()
        {
            var deltaAlignment = transform.InverseTransformPoint(target.CenterOfMass).x;
            transform.Rotate(Vector3.up * TurnRate * deltaAlignment);

            if (ReadyToFire)
            {
                viewPoint.LookAt(target.CenterOfMass);
                weapon.Fire();
            }
        }

        public void StopAiming()
        {
            animator.RestAim();
            enabled = false;
        }

        public void StartAiming()
        {
            animator.Aim();
            enabled = true;
        }
    }
}
