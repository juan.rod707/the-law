﻿using System;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Navigation;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class HostageController : MonoBehaviour
    {
        public Health Health;
        public Ragdoll Ragdoll;
        public Animator Animator;
        public ActorAnimator ActorAnimator;
        public Navigator Navigator;
        public HostageBrain Brain;
        public Interactable Interactable;

        private Action onDeath;
        private Action<HostageController> onExit;
        public bool IsAlive { get; private set; }

        public void Initialize(NavPointProvider navigationProvider, Vector3 spawnLocation, Action onDeath, Action<HostageController> onExit)
        {
            IsAlive = true;
            this.onDeath = onDeath;
            this.onExit = onExit;

            ActorAnimator.Initialize(Animator);
            Navigator.Initialize(ActorAnimator, spawnLocation);
            Ragdoll.Initialize();
            Health.Initialize(ActorAnimator.GetHit, OnDie);
            Brain.Initialize(Navigator, Interactable, ActorAnimator, navigationProvider, ExitLevel);
        }

        private void ExitLevel()
        { 
            gameObject.SetActive(false);
            onExit(this);
        }

        void OnDie()
        {
            Ragdoll.TurnOnRagdoll();
            Animator.enabled = false;
            Navigator.Shutdown();
            Brain.Shutdown();
            IsAlive = false;
            Destroy(Interactable);

            onDeath();
        }
    }
}
