﻿using System;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Navigation;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class CivilianController : MonoBehaviour
    {
        public Health Health;
        public Ragdoll Ragdoll;
        public Animator Animator;
        public ActorAnimator ActorAnimator;
        public Navigator Navigator;
        public CivilianBrain Brain;

        private Action onDeath;
        public bool IsAlive { get; private set; }

        public void Initialize(NavPointProvider navigationProvider, Vector3 spawnLocation, Action onDeath)
        {
            IsAlive = true;

            ActorAnimator.Initialize(Animator);
            Navigator.Initialize(ActorAnimator, spawnLocation);
            Ragdoll.Initialize();
            Health.Initialize(ActorAnimator.GetHit, OnDie);
            Brain.Initialize(Navigator, navigationProvider);

            this.onDeath = onDeath;
        }

        void OnDie()
        {
            Ragdoll.TurnOnRagdoll();
            Animator.enabled = false;
            Navigator.Shutdown();
            Brain.Shutdown();
            IsAlive = false;

            onDeath();
        }
    }
}
