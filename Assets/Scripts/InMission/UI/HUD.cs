﻿using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.InMission.UI
{
    public class HUD : MonoBehaviour
    {
        public GraphicCounter HealthBar;
        public TunnelVision TunnelVision;
        public HurtIndicator HurtIndicator;

        public DynamicLabel EnemyCount;
        public DynamicLabel CivilianDeaths;

        public Animator Fade;

        public void Initialize(int maxHp) => HealthBar.Initialize(maxHp, maxHp);

        public void UpdateHealth(int maxHp, int currentHp) => HealthBar.UpdateCounter(currentHp);

        public void ShowGameOver() => Debug.Log("YOU ARE DEAD");

        public void UpdateEnemies(int count) => EnemyCount.UpdateText(count);

        public void UpdateCivilianDeaths(int count) => CivilianDeaths.UpdateText(count);

        public void ShowFadeOut() => Fade.SetTrigger("FadeOut");
    }
}
